import React, { Component } from "react";

export default class ListKey extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listProduct: [
        { id: 1, name: "Iphone 12", price: 15000000 },
        { id: 2, name: "Oppo", price: 14000000 },
        { id: 3, name: "Hewei", price: 13000000 },
      ],
    };
  }
  renderTable =()=> {
      const {listProduct} = this.state;
    return listProduct.map((product)=>{
          //trả 1 mảng mới
            return (
                // trả về 3 thẻ tr
                <tr key = {product.id}>
                    <td>{product.id}</td>
                    <td>{product.name}</td>
                    <td>{product.price}</td>
                </tr>
            );
      });
  }

  render() {
    return (
      <div>
        <h3>ListKey</h3>
        <table className="table">
            <thead>
                <tr>
                    <td>id</td>
                    <td>name</td>
                    <td>price</td>
                </tr>
            </thead>
            <tbody>
                {this.renderTable()};
            </tbody>
        </table>
      </div>
    );
  }
}
