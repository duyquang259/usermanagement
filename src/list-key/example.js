import React, { Component } from 'react';
import data from "./data.json";

export default class ExampleListKey extends Component {
    constructor(props){
        super(props);
        this.state = {
            listMovie: data,
        };
    }
    renderListMovie = () =>{
     return this.state.listMovie.map((item)=>{
            return (
                <div key={item.maPhim} className="col-md-4">
                                <div className="card">
                                    <img className="card-img-top" src={item.hinhAnh}>
                                    </img>
                                    <div className="card-body">
                                        <h4 className="card-title">{item.tenPhim}</h4>
                                        <p className="card-text">{item.moTa}</p>
                                    </div>
                                </div>
                 </div>
                 )
        })
    };
    render() {
        return (
            <div className="container">
                {/* <h3>Example List Key</h3> */}
                <div className="row">
                    {this.renderListMovie()}
                </div>
            </div>
        )
    }
}
