//Nhúng file App.css để sử dụng
import "./App.css";

// import Baitap1 from "./baitap1/index";
// import Baitap2 from "./baitap2/index";
// import RenderingElements from "./rendering-elements/index";
// import HandlingEvent from "./handling-event/index"
// import Example from "./handling-event/example"
// import State from "./state/index";
// import ExampleCar from"./example-car/index";
// import ListKey from "./list-key/index";
// import ExampleListKey from "./list-key/example"
// import Props from "./props/index"
// import ShoppingCart from "./shopping-cart/index";
import Home from "./usermanagement/index";

function App() {
  return (
    <>
      {/* <Baitap1/>
     <Baitap2 />
    <RenderingElements/> */}
      {/* <HandlingEvent/>
       */}
      {/* <Example/>
       */}
      {/* <State/> */}
      {/* <ExampleCar/> */}
      {/* <ListKey/> */}
      {/* <ExampleListKey/> */}
      {/* <Props/> */}
      {/* <ShoppingCart /> */}
      <Home />
    </>
  );
}

export default App;
