import React, { Component } from "react";
import Search from "./Search";
import Users from "./Users";
import Modal from "./Modal";
import data from "./data.json"

class Home extends Component {
  constructor(props){
    super(props);
    this.state={
      listUser: data,
    };
  }

  handleGetDetailUser = (user) =>{
    this.setState({
      detailUser: user,
    });
  };

  //Ham tim index
  _findIndex = (id) =>{
    return this.state.listUser.findIndex((item)=>{
      return item.id === id;
    });
  };

  //ham xoa User
  handleDeleteUser = (user) =>{
    console.log(user);
    let viTri = this._findIndex(user.id);
    let listUSER = this.state.listUser;
    if(viTri !== -1){
      //Xoa
      listUSER.splice(viTri,1);
    }
    this.setState({listUSER});
  }
  render() {
    return (
      <div className="container d-flex flex-column">
        <div>
            <h1 className="display-4 text-center my-3">User Management</h1>
            <div className="d-flex justify-content-around align-items-center">
              <Search />
              <button
                className="btn btn-success"
                data-toggle="modal"
                data-target="#modelIdUser"
              >
                Add User
              </button>
            </div>
        </div>
        <div>
          <Users 
          listUser = {this.state.listUser}
          getDetailUser={this.handleGetDetailUser}
          getDeleteUser={this.handleDeleteUser}
          />
          <Modal />
        </div>
      </div>
    );
  }
}

export default Home;
