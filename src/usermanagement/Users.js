import React, { Component } from "react";
import UserItem from "./UserItem";

class Users extends Component {
  renderListUser = () => {
    const { listUser } = this.props;
    return listUser.map((item) => {
      return (
      
          <UserItem
          key={item.id}
          user={item}
          getDetailUser={this.props.getDetailUser}
          getDeleteUser={this.props.getDeleteUser}
        />
      );
    });
  };

  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Username</th>
              <th>Email</th>
              <th>Phone Number</th>
              <th>Type</th>
            </tr>
          </thead>
          <tbody>
                {this.renderListUser()}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Users;
