import React, { Component } from "react";

export default class ExampleCar extends Component {
    constructor(props){
        super(props);
        this.state  = {
            img : "./images/imgRedCar.jpg"
        }
    }
    //Hàm thay đổi hình xe
    changeColor = (img) =>{
        //SwitchCase
        // let img = "";
        // switch (color){
        //     case "red":
        //         img = "./images/imgRedCar.jpg"
        //         break;
        //     case "silver":
        //         img = "./images/imgSilverCar.jpg"
        //         break;
        //     case "black":
        //         img = "./images/imgBlackCar.jpg"
        //         break;
        //         default:
        //                 break;
        // }
        this.setState({img});
    }

  render() {
      //Bóc  tách thuộc tính img  kiểu đối tượng trong this.state
      const  {img}= this.state;
    return (
      <div className="container">
        <h3>*ExampleCar</h3>
        <div className="row">
            <div className="col-6">
                <img className="img-fluid" src={img} alt="1"/>
            </div>
            <div className="col-6">
                {/* SWITCH CASE
                <button className="btn btn-danger " onClick={()=>{this.changeColor("red")}}>Red Car</button>
                <button className="btn btn-light mx-2" onClick={()=>{this.changeColor("silver")}}>Silver Car</button>
                <button className="btn btn-dark" onClick={()=>{this.changeColor("black")}}>Black Car</button> */}
        
                <button className="btn btn-danger " onClick={()=>{this.changeColor("./images/imgRedCar.jpg")}}>Red Car</button>
                <button className="btn btn-light mx-2" onClick={()=>{this.changeColor("./images/imgSilverCar.jpg")}}>Silver Car</button>
                <button className="btn btn-dark" onClick={()=>{this.changeColor("./images/imgBlackCar.jpg")}}>Black Car</button>
            </div>
        </div>
      </div>
    );
  }
}
