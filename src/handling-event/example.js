import React, { Component } from "react";

export default class Example extends Component {
  // kiểm tra login hay chưa
  //tạo biến kiểm tra login
  isLogin = false;
  handleLogin = () => {
    this.isLogin = true;
    console.log(this.isLogin);
  };
  showInfo = () => {
    // if(this.isLogin) {
    //     return  <h1>Hello Cybersoft</h1> ;
    //     //các thẻ không được đồng cấp, vậy phải bọc trong 1 thẻ trống, hoặc div
    //     }
    //      else
    //      {
    //        return  <button className="btn btn-primary">Login</button>
    // }
    //Toán tử 3 ngôi: nếu đúng thì show
    return this.isLogin ? (
      <h1>Hello Cybersoft</h1>
    ) : (
      <button className="btn btn-primary" onClick={this.handleLogin}>
        Login
      </button>
    );
  };
  render() {
    return (
      <div>
        <h3>*ExampleHandlingEvent</h3>
        {this.showInfo()}
      </div>
    );
  }
}
