import React, { Component } from "react";

export default class HandlingEvent extends Component {

    //Hàm
handleClick = ()=>{
    console.log(123);
}

handleClickParams = (name, lop) =>{
    console.log(name, lop);
}
  render() {
    return (
      <div>
        <h3>Handling Event</h3>
        {/* Không để this.handleClick() vì nó sẽ chạy luôn, muốn click  1 lần thì phải bỏ () */}
        <button className="btn btn-success" onClick={this.handleClick}>Click</button>
        <button className = "btn btn-danger" onClick={()=>{this.handleClickParams("Quang", "BCO3")}}> Click Params</button>
      </div>
    );
  }
}
