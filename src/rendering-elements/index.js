import React, { Component } from "react";

export default class RenderingElements extends Component {
  // tạo biến
  username = "Quang";
  lop = "BC03";

  //phương thức
  getInfo = () => {
    return (
      <p>
        Username : {this.username} - Lop: {this.lop}
      </p>
    );
  };
  render() {
    return (
      <div>
        <h3> *Rendering Elements</h3>
        {/*gọi lại hàm */}
        {this.getInfo()}
      </div>
    );
  }
}
