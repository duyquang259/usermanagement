//tạo component
import React, { Component } from "react";
import Header from "./header";
import Content from "./content";
import Sidebar from "./sidebar";
import Footer from "./footer";

//Tạo đối tượng Baitap1 kế thừa theo Component => Baitap1 sẽ thành 1 component
class Baitap1 extends Component {
    //vì kế thừa component sẽ sử dụng render: tất cả các tag trong HTML
    render(){
        //trả về các thẻ trong HTML
        return(
            //tạo thẻ ảo để tránh nhiều thẻ div
            <> 
                    {/* các thẻ cùng cấp thẻ khuyết- phải bọc trong 1 thẻ chứ không JSX không cho */}
                < Header/>
                <div className="container">
                    <Content/>
                    <Sidebar/>
                </div>
                
                <Footer/>
            </>
           
        )
    }
}

export default  Baitap1;