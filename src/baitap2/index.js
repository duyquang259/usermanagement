//Stateless: ES5
import React from "react";
import Header from "./header";
import Carousel from "./carousel";
import Contact from "./contact";
import WhatWeDo from "./what-we-do";
import ListCard from "./listcard";
import Footer from "./footer"
import CardItem from "./card-item"
function Baitap2() {
    return (
        <>
            <Header/>
            <Carousel/>
            <div className="container">
                <div className ="row">
                    <WhatWeDo/>
                    <Contact/>
                </div>
            </div>
            <ListCard />
            <Footer/>
        </>
    )
}
export default Baitap2;
