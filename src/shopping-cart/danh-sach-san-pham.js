import React, { Component } from "react";
import SanPham from "./san-pham";

export default class DanhSachSanPham extends Component {
  renderListProduct = () => {
    // const {listProduct} = this.props;
    // this.props.listProduct.map((item) => {
    //   return <SanPham key="{item.maSP}" />;
    // });
    const { listProduct } = this.props;
    return listProduct.map((item) => {
      // item là đối tượng trong mảng
      return (
      <SanPham 
      key={item.maSP} 
      product={item} 
      getDetailProduct={this.props.getDetailProduct} 
      //this.props từ index truyền vào:từ cha truyền cho con
      getProductAddCart ={this.props.getProductAddCart}/>
    );
  });
  };
  render() {
    return (
      <div className="container">
        <div className="row">{this.renderListProduct()}</div>
      </div>
    );
  }
}
