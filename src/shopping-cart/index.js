import React, { Component } from "react";
import DanhSachSanPham from "./danh-sach-san-pham";
import Modal from "./modal";
import data from "./data.json";

export default class ShoppingCart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listProduct: data,
      detailProduct: data[0],
      listCart: [],
    };
  }
  handleGetDetailProduct = (product) => {
    this.setState({
      detailProduct: product,
    });
  };
  //Hàm tìm Index
  _findIndex = (maSP) => {
    //Tìm index
    return this.state.listCart.findIndex((item) => {
      return item.maSP === maSP;
    });
  };

  //hàm getInfo SP
  handleGetProductAddCart = (product) => {
    console.log(product);
    // thêm sản phẩm vào state.listCart
    //thêm quantity vào listCart
    let productNew = {
      maSP: product.maSP,
      tenSP: product.tenSP,
      hinhAnh: product.hinhAnh,
      soLuong: 1,
      giaBan: product.giaBan,
    };
    //lấy những sp từ trên xuống để add vào listCart
    let listCart = [...this.state.listCart];

    // //Tìm index
    // let viTri = listCart.findIndex((item) => {
    //   return item.maSP === productNew.maSP;
    // });

    //Gọi hàm _findIndex
    let viTri = this._findIndex(productNew.maSP);
    console.log(viTri);
    // if (viTri !== -1)

    if (viTri !== -1) {
      //Nếu như sản phẩm tồn tại trong giỏ hàng thì cập nhật số lượng
      listCart[viTri].soLuong += 1;
    } else {
      //Thêm sp vào listCart
      //... tạo ra mảng mới copy từ this.state.lisCart,
      listCart = [...this.state.listCart, productNew];
    }
    this.setState({
      listCart: listCart,
    });
  };

  //Xóa product trong mảng listCart
  handleDeleteProduct = (product) => {
    console.log(product);
    let viTri = this._findIndex(product.maSP);
    let listCart = [...this.state.listCart];
    if (viTri !== -1) {
      //Xóa
      listCart.splice(viTri, 1);
    }
    //xóa xong phải set lại listCart
    this.setState({ listCart });
  };

  //Hàm update Quantity
  handleUpdateQuantity = (product, status) => {
    // 0. Copy lại mảng this.state.listCart
    //1. Tìm Vị trí
    //2.So sánh status là true =>tăng
    //Ngược lại: => Giảm
    //3. setState
    let listCart = [...this.state.listCart];
    let viTri = this._findIndex(product.maSP);
    if (viTri !== -1) {
      if (status === true) {
        //Tăng
        listCart[viTri].soLuong += 1;
      } else {
        //Giảm
        if (listCart[viTri].soLuong > 1) {
          listCart[viTri].soLuong -= 1;
        }
      }
    }
    this.setState({ listCart });

    //handleShowQuantity
  };
  handleShowQuantity = () => {
    let listCart = [...this.state.listCart];
    //Có thể sử dụng hàm Reduce để bỏ đi biến total
    let total = 0;
    listCart.forEach((item) => {
      total += item.soLuong;
    });
    return total;
  };
  render() {
    const { detailProduct } = this.state;
    return (
      <div>
        <h3 className="title">Bài tập giỏ hàng</h3>
        <div className="container">
          <button
            className="btn btn-danger"
            data-toggle="modal"
            data-target="#modelId"
          >
            Giỏ hàng ({this.handleShowQuantity()})
          </button>
        </div>
        <DanhSachSanPham
          listProduct={this.state.listProduct}
          getDetailProduct={this.handleGetDetailProduct}
          getProductAddCart={this.handleGetProductAddCart}
        />
        {/* truyền info listCart vào modal */}
        <Modal
          listCart={this.state.listCart}
          getProductDelete={this.handleDeleteProduct}
          getProductUpdateQuantity={this.handleUpdateQuantity}
        />
        <div className="row">
          <div className="col-sm-5">
            <img className="img-fluid" src={detailProduct.hinhAnh} alt="" />
          </div>
          <div className="col-sm-7">
            <h3>Thông số kỹ thuật</h3>
            <table className="table">
              <tbody>
                <tr>
                  <td>Màn hình</td>
                  <td>{detailProduct.manHinh}</td>
                </tr>
                <tr>
                  <td>Hệ điều hành</td>
                  <td>{detailProduct.heDieuHanh}</td>
                </tr>
                <tr>
                  <td>Camera trước</td>
                  <td>{detailProduct.cameraTruoc}</td>
                </tr>
                <tr>
                  <td>Camera sau</td>
                  <td>{detailProduct.cameraSau}</td>
                </tr>
                <tr>
                  <td>RAM</td>
                  <td>{detailProduct.ram}</td>
                </tr>
                <tr>
                  <td>ROM</td>
                  <td>{detailProduct.rom}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
