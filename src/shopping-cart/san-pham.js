import React, { Component } from "react";

export default class SanPham extends Component {
  handleDetail = () => {
    // console.log(this.props.product);
    this.props.getDetailProduct(this.props.product);
  };

  // handleGetProductAddCart = () =>{
  //   this.props.getProductAddCart(this.props.product);
  // }

  handle;
  render() {
    const { product } = this.props;
    return (
      <div className="col-sm-4">
        <div className="card">
          <img className="card-img-top" src={product.hinhAnh} alt="1" />
          <div className="card-body">
            <h4 className="card-title">{product.tenSP}</h4>
            <button className="btn btn-success" onClick={this.handleDetail}>
              Chi tiết
            </button>
            {/* viết tắt hàm bằng 1 callback function với 1 dòng lệnh */}
            <button
              className="btn btn-danger"
              onClick={() => {
                this.props.getProductAddCart(product);
              }}
            >
              Thêm giỏ hàng
            </button>
          </div>
        </div>
      </div>
    );
  }
}
