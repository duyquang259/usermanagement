import React from 'react'
//tham so props trong Function
export default function ChildFunction(props) {

    return (
        <div>
            <h3>Child Function</h3>
            {/* khong co this */}
            <p>Username Props: {props.dataUsername} - Lop: {props.dataLop}</p>
        </div>
    )
}

