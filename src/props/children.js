import React from "react";

export default function Children(props) {
  return (
    <div>
      <h3>Children</h3>
      {/* props.children có sẵn (mặc định) 
            trong React, Truyền giữa Ông với Cháu
            */}
      {props.children}
    </div>
  );
}
