import React, {Component } from 'react'
import Child from "./child"
import ChildFunction from "./childFunction"
import Children from "./children"


export default class Props extends Component {
    constructor(props){
        super(props);
        this.state = {
            username: "Cybersoft",
            lop: "BC03-35"
        };
    }
    handleChangeUsername =()=>{
        console.log(123);
        this.setState({
                username: "Quang",
                lop: "BC03 - 24"
            }
        )
    }

    handleGetResetUsername = (resetUser)=>{
        this.setState({
           username: resetUser,
        });
    }
    render() {
        return (
            <div>
                <h1>Props</h1>
                <p>Username: {this.state.username}</p>
                <p>Lop: {this.state.lop}</p>
                {/* đặt tên Props mang giá trị từ cha lấy xuống chiều từ Cha-Con
                data: đặt sao cũng được
                */}
                <button className="btn btn-success" onClick = {this.handleChangeUsername}>Change Username</button>
               <br></br>*********************************************
                <Child dataUsername ={this.state.username} dataLop = {this.state.lop} getResetUsername={this.handleGetResetUsername}/>
                <ChildFunction dataUsername ={this.state.username} dataLop = {this.state.lop}/>
                {/*Muốn truyền thẻ html phải có thẻ mở đóng */}
                <Children>
                    <div>
                        <p>Demo</p>
                        <p>Lorem</p>
                    </div>
                </Children>
            </div>
        )
    }
}
