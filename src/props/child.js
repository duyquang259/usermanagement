import React, { Component } from 'react'

export default class Child extends Component {
    constructor(props){
        super(props);
        this.state = {
            username : "Quang",
        };
    }

    handleReset = ()=>{
       
        const resetUser ="Cybersoft";
        // console.log(resetUser);
        this.props.getResetUsername(resetUser);
    }
    render() {
        return (
            <div>
                <h3>Child</h3>
                {/* từ khóa this.props mặc định: nhận lại giá trị từ Cha truyền qua */}
                <p>Username Props: {this.props.dataUsername}</p>
                <p>Username 2: {this.state.username}</p>
                <p>Lớp Props: {this.props.dataLop}</p>
                <button className="btn btn-primary" onClick={this.handleReset}>Reset Username</button>
            </div>
        )
    }
}
