import React, { Component } from "react";

export default class State extends Component {
  isLogin = false;
  //Hàm khởi tạo đối tượng
  constructor(props) {
    super(props);
    this.state = {
      isLogin: false,
    };
  }
  handleLogin = () => {
    //cập nhật lại state thông qua hàm chứ không qua dấu =
    // this.state.isLogin = true;
    //Hàm setState bất đồng bộ nên đọc sau
    this.setState(
      {
        isLogin: true,
      },
      //Tham số thứ 2 là callback function
      () => {
        console.log(this.state.isLogin); //true
      }
    );
    //console.log(this.state.isLogin); // chạy trước nên kết quả false
  };
  showInfo = () => {
    return this.state.isLogin ? (
      <h1>Hello Cybersoft</h1>
    ) : (
      <button className="btn btn-primary" onClick={this.handleLogin}>
        Login
      </button>
    );
  };
  render() {
    console.log("render");
    return (
      <div>
        <h3>*State</h3>
        {this.showInfo()}
      </div>
    );
  }
}

//Khi nào render chạy lại?
//Khi setState bị thay đổi thì render chạy lại (tùy theo state thay đổi bao nhiêu lần)
// chỉ có thẻ nào có state bị thay đổi thì sẽ render lại, còn component bt không render lại
